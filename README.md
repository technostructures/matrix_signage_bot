# Matrix Signage Bot

## Development environment

### Using [Docker](https://docs.docker.com/engine/install/)

```bash
git clone git@gitlab.com:technostructures/matrix_signage_bot.git
cd matrix_signage_bot
docker compose run --rm synapse generate
docker compose run --rm msb mix do deps.get, ecto.setup
docker compose up
```

On Linux, use `docker-hoster` to make container domains accessible:
```
docker run -d \
    -v /var/run/docker.sock:/tmp/docker.sock \
    -v /etc/hosts:/tmp/hosts \
    --name docker-hoster \
    dvdarias/docker-hoster@sha256:2b0e0f8155446e55f965fa33691da828c1db50b24d5916d690b47439524291ba
```

(after rebooting, you will need to start it again using `docker start docker-hoster`)

This should run containers with those services:

- [msb.msb.local](http://msb.msb.local) -> Matrix Signage Bot
- [matrix.msb.local](http://matrix.msb.local) -> Synapse
- [msb.local](http://msb.local) -> Delegation server to use `:msb.local` addresses
- [element.msb.local](http://element.msb.local) -> Element configured to connect with Synapse

You can also start an IEx console:

```bash
docker compose run --rm msb iex -S mix
```
