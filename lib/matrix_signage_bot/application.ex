defmodule MatrixSignageBot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      MatrixSignageBotWeb.Telemetry,
      # Start the Ecto repository
      MatrixSignageBot.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: MatrixSignageBot.PubSub},
      # Start Finch
      {Finch, name: MatrixSignageBot.Finch},
      # Start the Endpoint (http/https)
      MatrixSignageBotWeb.Endpoint,
      # Start a worker by calling: MatrixSignageBot.Worker.start_link(arg)
      # {MatrixSignageBot.Worker, arg}
      Polyjuice.Client.child_spec(MatrixSignageBot.Bot.client_options())
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MatrixSignageBot.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    MatrixSignageBotWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
