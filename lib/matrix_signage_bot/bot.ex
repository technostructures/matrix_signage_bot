defmodule MatrixSignageBot.Bot do
  require Logger

  @client_name MatrixSignageBot.Bot
  @pubsub MatrixSignageBot.PubSub

  def client_options() do
    [
      Application.fetch_env!(:matrix_signage_bot, :matrix_url),
      name: @client_name,
      access_token: Application.fetch_env!(:matrix_signage_bot, :access_token),
      user_id: Application.fetch_env!(:matrix_signage_bot, :user_id),
      storage: Polyjuice.Client.Storage.Ets.open(),
      handler: &MatrixSignageBot.Bot.handle_event/2,
      sync_filter:
        Polyjuice.Client.Filter.include_timeline_types(["m.reaction", "m.room.redaction"])
        |> Polyjuice.Client.Filter.include_state_types([])
        |> Polyjuice.Client.Filter.include_ephemeral_types([])
        |> Polyjuice.Client.Filter.include_presence_types([])
    ]
  end

  def client(), do: Polyjuice.Client.get_client(@client_name)

  def handle_event(
        :message,
        {room_id,
         %{
           "type" => "m.reaction",
           "content" => %{
             "m.relates_to" => %{
               "rel_type" => "m.annotation",
               "event_id" => event_id,
               "key" => emoji
             }
           },
           "event_id" => reaction_id,
           "origin_server_ts" => _reaction_sent_at,
           "sender" => reactor_id
         }}
      ) do
    Logger.debug("received a reaction")

    case category(room_id, emoji) do
      nil ->
        :ok

      {category_slug, category} ->
        Logger.debug("found a corresponding category")

        if has_right?(room_id, reactor_id, category) do
          Logger.debug("user is allowed to create an entry")

          {:ok,
           %{
             "type" => "m.room.message",
             "content" => content,
             "origin_server_ts" => sent_at,
             "sender" => author_id
           }} = Polyjuice.Client.Room.get_event(client(), room_id, event_id) |> dbg()

          {:ok, sent_at} = DateTime.from_unix(sent_at, :millisecond)

          case MatrixSignageBot.Entries.create_entry(%{
                 category_name: category[:name],
                 category_slug: to_string(category_slug),
                 author_id: author_id,
                 # author_name: author_name,
                 room_id: room_id,
                 # room_name: room_name,
                 body: Map.get(content, "formatted_body") || Map.get(content, "body"),
                 sent_at: sent_at,
                 event_id: event_id,
                 reaction_id: reaction_id
               }) do
            {:ok, entry} ->
              Phoenix.PubSub.broadcast(@pubsub, "category:#{category_slug}", {:new_entry, entry})

            {:error, error} ->
              dbg(error)
              :ok
          end
        end
    end
  end

  def handle_event(
        :message,
        {_room_id,
         %{
           "type" => "m.room.redaction",
           "content" => %{},
           "redacts" => reaction_id
         }}
      ) do
    case MatrixSignageBot.Entries.get_entry_by_reaction_id(reaction_id) do
      nil ->
        :ok

      entry ->
        case MatrixSignageBot.Entries.delete_entry(entry) do
          {:ok, %{category_slug: category_slug} = entry} ->
            Phoenix.PubSub.broadcast(@pubsub, "category:#{category_slug}", {:delete_entry, entry})

          {:error, error} ->
            dbg(error)
            :ok
        end
    end
  end

  def handle_event(type, args) do
    dbg({type, args})
  end

  defp category(room_id, emoji) do
    Application.fetch_env!(:matrix_signage_bot, :categories)
    |> Enum.find(fn {_category_slug, category} ->
      category[:emoji] == emoji && room_id in category[:rooms]
    end)
  end

  defp has_right?(room_id, user_id, %{power_level: power_level}) do
    case power_level_for_user(room_id, user_id) do
      n when is_integer(n) ->
        n >= power_level

      _ ->
        false
    end
  end

  defp power_level_for_user(room_id, user_id) do
    case Polyjuice.Client.Room.get_state(client(), room_id, "m.room.power_levels", "") do
      {:ok, %{"users" => users}} ->
        Map.get(users, user_id)

      _ ->
        nil
    end
  end
end
