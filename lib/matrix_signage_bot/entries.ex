defmodule MatrixSignageBot.Entries do
  @moduledoc """
  The Entries context.
  """

  import Ecto.Query, warn: false
  alias MatrixSignageBot.Repo

  alias MatrixSignageBot.Entries.Entry

  @doc """
  Returns the list of entries.

  ## Examples

      iex> list_entries()
      [%Entry{}, ...]

  """
  def list_entries do
    Repo.all(Entry)
  end

  @doc """
  Returns the list of entries for a `category_slug`.

  ## Examples

      iex> list_entries_for_category("fuz_events")
      [%Entry{}, ...]

  """
  def list_entries_for_category(category_slug) do
    Entry |> where(category_slug: ^category_slug) |> Repo.all() |> dbg()
  end

  @doc """
  Gets a single entry.

  Raises `Ecto.NoResultsError` if the Entry does not exist.

  ## Examples

      iex> get_entry!(123)
      %Entry{}

      iex> get_entry!(456)
      ** (Ecto.NoResultsError)

  """
  def get_entry!(id), do: Repo.get!(Entry, id)

  @doc """
  Gets a single entry by `event_id`.

  ## Examples

      iex> get_entry_by_event_id(123)
      %Entry{}

      iex> get_entry_by_event_id(456)
      nil

  """
  def get_entry_by_event_id(event_id), do: Repo.get_by(Entry, event_id: event_id)

  @doc """
  Gets a single entry by `reaction_id`.

  ## Examples

      iex> get_entry_by_reaction_id(123)
      %Entry{}

      iex> get_entry_by_reaction_id(456)
      nil

  """
  def get_entry_by_reaction_id(reaction_id), do: Repo.get_by(Entry, reaction_id: reaction_id)

  @doc """
  Creates a entry.

  ## Examples

      iex> create_entry(%{field: value})
      {:ok, %Entry{}}

      iex> create_entry(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_entry(attrs \\ %{}) do
    %Entry{}
    |> Entry.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a entry.

  ## Examples

      iex> update_entry(entry, %{field: new_value})
      {:ok, %Entry{}}

      iex> update_entry(entry, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_entry(%Entry{} = entry, attrs) do
    entry
    |> Entry.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a entry.

  ## Examples

      iex> delete_entry(entry)
      {:ok, %Entry{}}

      iex> delete_entry(entry)
      {:error, %Ecto.Changeset{}}

  """
  def delete_entry(%Entry{} = entry) do
    Repo.delete(entry)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking entry changes.

  ## Examples

      iex> change_entry(entry)
      %Ecto.Changeset{data: %Entry{}}

  """
  def change_entry(%Entry{} = entry, attrs \\ %{}) do
    Entry.changeset(entry, attrs)
  end
end
