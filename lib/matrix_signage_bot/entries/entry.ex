defmodule MatrixSignageBot.Entries.Entry do
  use Ecto.Schema
  import Ecto.Changeset

  schema "entries" do
    field :author_id, :string
    field :author_name, :string
    field :body, :string
    field :category_name, :string
    field :category_slug, :string
    field :event_id, :string
    field :reaction_id, :string
    field :room_id, :string
    field :room_name, :string
    field :sent_at, :naive_datetime_usec

    timestamps()
  end

  @doc false
  def changeset(entry, attrs) do
    entry
    |> cast(attrs, [
      :category_name,
      :category_slug,
      :author_id,
      :author_name,
      :room_id,
      :room_name,
      :body,
      :sent_at,
      :event_id,
      :reaction_id
    ])
    |> validate_required([
      :category_name,
      :category_slug,
      :author_id,
      # :author_name,
      :room_id,
      # :room_name,
      :body,
      :sent_at,
      :event_id,
      :reaction_id
    ])
    |> unique_constraint(:event_id)
  end
end
