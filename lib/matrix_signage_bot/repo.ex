defmodule MatrixSignageBot.Repo do
  use Ecto.Repo,
    otp_app: :matrix_signage_bot,
    adapter: Ecto.Adapters.Postgres
end
