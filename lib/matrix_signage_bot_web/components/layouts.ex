defmodule MatrixSignageBotWeb.Layouts do
  use MatrixSignageBotWeb, :html

  embed_templates "layouts/*"
end
