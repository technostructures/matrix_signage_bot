defmodule MatrixSignageBotWeb.PageHTML do
  use MatrixSignageBotWeb, :html

  embed_templates "page_html/*"
end
