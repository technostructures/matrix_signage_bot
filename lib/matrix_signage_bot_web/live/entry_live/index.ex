defmodule MatrixSignageBotWeb.EntryLive.Index do
  use MatrixSignageBotWeb, :live_view

  alias MatrixSignageBot.Entries
  alias MatrixSignageBot.Entries.Entry

  @pubsub MatrixSignageBot.PubSub

  @impl true
  def mount(%{"category_slug" => category_slug}, _session, socket) do
    category =
      Application.fetch_env!(:matrix_signage_bot, :categories)[String.to_atom(category_slug)]

    if connected?(socket), do: Phoenix.PubSub.subscribe(@pubsub, "category:#{category_slug}")

    {:ok,
     socket
     |> assign(:page_title, category[:name])
     |> assign(:category, category)
     |> stream(:entries, Entries.list_entries_for_category(category_slug))}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, socket}
  end

  def handle_info({:new_entry, entry}, socket) do
    {:noreply, stream_insert(socket, :entries, entry)}
  end

  def handle_info({:delete_entry, entry}, socket) do
    {:noreply, stream_delete(socket, :entries, entry)}
  end
end
