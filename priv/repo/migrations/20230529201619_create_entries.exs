defmodule MatrixSignageBot.Repo.Migrations.CreateEntries do
  use Ecto.Migration

  def change do
    create table(:entries) do
      add :category_name, :string
      add :category_slug, :string
      add :event_id, :string
      add :reaction_id, :string
      add :author_id, :string
      add :author_name, :string
      add :room_id, :string
      add :room_name, :string
      add :body, :text
      add :sent_at, :naive_datetime_usec

      timestamps()
    end

    create(unique_index(:entries, [:event_id]))
  end
end
