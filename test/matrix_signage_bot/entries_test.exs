defmodule MatrixSignageBot.EntriesTest do
  use MatrixSignageBot.DataCase

  alias MatrixSignageBot.Entries

  describe "entries" do
    alias MatrixSignageBot.Entries.Entry

    import MatrixSignageBot.EntriesFixtures

    @invalid_attrs %{
      author_id: nil,
      author_name: nil,
      body: nil,
      category_name: nil,
      category_slug: nil,
      room_id: nil,
      entry_id: nil,
      reaction_id: nil,
      room_name: nil,
      sent_at: nil
    }

    test "list_entries/0 returns all entries" do
      entry = entry_fixture()
      assert Entries.list_entries() == [entry]
    end

    test "get_entry!/1 returns the entry with given id" do
      entry = entry_fixture()
      assert Entries.get_entry!(entry.id) == entry
    end

    test "create_entry/1 with valid data creates a entry" do
      valid_attrs = %{
        author_id: "some author_id",
        author_name: "some author_name",
        body: "some body",
        category_name: "some category_name",
        category_slug: "some category_slug",
        room_id: "some room_id",
        event_id: "some event_id",
        reaction_id: "some reaction_id",
        room_name: "some room_name",
        sent_at: ~N[2023-05-28 20:16:00.000000]
      }

      assert {:ok, %Entry{} = entry} = Entries.create_entry(valid_attrs)
      assert entry.author_id == "some author_id"
      assert entry.author_name == "some author_name"
      assert entry.body == "some body"
      assert entry.category_name == "some category_name"
      assert entry.category_slug == "some category_slug"
      assert entry.room_id == "some room_id"
      assert entry.entry_id == "some entry_id"
      assert entry.reaction_id == "some reaction_id"
      assert entry.room_name == "some room_name"
      assert entry.sent_at == ~N[2023-05-28 20:16:00.000000]
    end

    test "create_entry/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Entries.create_entry(@invalid_attrs)
    end

    test "update_entry/2 with valid data updates the entry" do
      entry = entry_fixture()

      update_attrs = %{
        author_id: "some updated author_id",
        author_name: "some updated author_name",
        body: "some updated body",
        category_name: "some updated category_name",
        category_slug: "some updated category_slug",
        room_id: "some updated room_id",
        entry_id: "some updated entry_id",
        reaction_id: "some updated reaction_id",
        room_name: "some updated room_name",
        sent_at: ~N[2023-05-29 20:16:00.000000]
      }

      assert {:ok, %Entry{} = entry} = Entries.update_entry(entry, update_attrs)
      assert entry.author_id == "some updated author_id"
      assert entry.author_name == "some updated author_name"
      assert entry.body == "some updated body"
      assert entry.category_name == "some updated category_name"
      assert entry.category_slug == "some updated category_slug"
      assert entry.room_id == "some updated room_id"
      assert entry.entry_id == "some updated entry_id"
      assert entry.reaction_id == "some updated reaction_id"
      assert entry.room_name == "some updated room_name"
      assert entry.sent_at == ~N[2023-05-29 20:16:00.000000]
    end

    test "update_entry/2 with invalid data returns error changeset" do
      entry = entry_fixture()
      assert {:error, %Ecto.Changeset{}} = Entries.update_entry(entry, @invalid_attrs)
      assert entry == Entries.get_entry!(entry.id)
    end

    test "delete_entry/1 deletes the entry" do
      entry = entry_fixture()
      assert {:ok, %Entry{}} = Entries.delete_entry(entry)
      assert_raise Ecto.NoResultsError, fn -> Entries.get_entry!(entry.id) end
    end

    test "change_entry/1 returns a entry changeset" do
      entry = entry_fixture()
      assert %Ecto.Changeset{} = Entries.change_entry(entry)
    end
  end
end
