defmodule MatrixSignageBotWeb.EntryLiveTest do
  use MatrixSignageBotWeb.ConnCase

  import Phoenix.LiveViewTest
  import MatrixSignageBot.EntriesFixtures

  @create_attrs %{
    author_id: "some author_id",
    author_name: "some author_name",
    body: "some body",
    category_name: "some category_name",
    category_slug: "some category_slug",
    room_id: "some room_id",
    room_name: "some room_name",
    sent_at: "2023-05-28T20:16:00.000000"
  }
  @update_attrs %{
    author_id: "some updated author_id",
    author_name: "some updated author_name",
    body: "some updated body",
    category_name: "some updated category_name",
    category_slug: "some updated category_slug",
    room_id: "some updated room_id",
    room_name: "some updated room_name",
    sent_at: "2023-05-29T20:16:00.000000"
  }
  @invalid_attrs %{
    author_id: nil,
    author_name: nil,
    body: nil,
    category_name: nil,
    category_slug: nil,
    room_id: nil,
    room_name: nil,
    sent_at: nil
  }

  defp create_entry(_) do
    entry = entry_fixture()
    %{entry: entry}
  end

  describe "Index" do
    setup [:create_entry]

    test "lists all entries", %{conn: conn, entry: entry} do
      {:ok, _index_live, html} = live(conn, ~p"/entries")

      assert html =~ "Listing Entries"
      assert html =~ entry.author_id
    end
  end
end
