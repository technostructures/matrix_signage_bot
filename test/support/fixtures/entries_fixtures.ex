defmodule MatrixSignageBot.EntriesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `MatrixSignageBot.Entries` context.
  """

  @doc """
  Generate a entry.
  """
  def entry_fixture(attrs \\ %{}) do
    {:ok, entry} =
      attrs
      |> Enum.into(%{
        author_id: "some author_id",
        author_name: "some author_name",
        body: "some body",
        category_name: "some category_name",
        category_slug: "some category_slug",
        event_id: "some event_id",
        reaction_id: "some reaction_id",
        room_id: "some room_id",
        room_name: "some room_name",
        sent_at: ~N[2023-05-28 20:16:00.000000]
      })
      |> MatrixSignageBot.Entries.create_entry()

    entry
  end
end
